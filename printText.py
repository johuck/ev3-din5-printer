#!/usr/bin/env micropython
import draw
import textwrap
import math

scale = 1.25
margin = 15


def print_text(text):
    for char in text.upper():
        switcher={
            'A':print_A,
            'B':print_B,
            'C':print_C,
            'D':print_D,
            'E':print_E,
            'F':print_F,
            'G':print_G,
            'H':print_H,
            'I':print_I,
            'J':print_J,
            'K':print_K,
            'L':print_L,
            'M':print_M,
            'N':print_N,
            'O':print_O,
            'P':print_P,
            'Q':print_Q,
            'R':print_R,
            'S':print_S,
            'T':print_T,
            'U':print_U,
            'V':print_V,
            'W':print_W,
            'X':print_X,
            'Y':print_Y,
            'Z':print_Z,
            '.':print_dot,
            ' ':print_space,
            '\n':print_linebreak
        }

        # if draw.penPositionX + 30*scale > 1000 and char != '.':
        #     print_linebreak()

        print_char=switcher.get(char)
        if print_char is None:
            print_char = print_N
        print_char()


def wrap(text):
    max_line_length = math.floor(1000/(scale*30)-2)
    print("max line length: " + str(max_line_length))
    wraped = textwrap.wrap(text, max_line_length, break_long_words=True)
    result = ""
    for line in wraped:
        result += '\n' + line
    return result


def getText():
    text = input("Bitte gib den zu druckenden Text ein: ")
    print_text(wrap(text))


def font_draw(x, y, pen):
    draw.penMove(x*scale, y*scale, pen)

def print_A():
    font_draw(25,50, True)
    font_draw(25, -50, True)
    font_draw(-12.5, 25, False)
    font_draw(-25, 0, True)
    font_draw(50, -25, False)

def print_B():
    font_draw(0, 50, True)
    font_draw(25, 0, True)
    font_draw(10, -10, True)
    font_draw(0, -5, True)
    font_draw(-10, -10, True)
    font_draw(-25, 0, True)
    font_draw(25, 0, False)
    font_draw(10, -10, True)
    font_draw(0, -5, True)
    font_draw(-10, -10, True)
    font_draw(-25, 0, True)
    font_draw(50, 0, False)

def print_C():
    font_draw(20, 10, False)
    font_draw(-10, -10, True)
    font_draw(-10, 0, True)
    font_draw(-10, 10, True)
    font_draw(0, 30, True)
    font_draw(10, 10, True)
    font_draw(10, 0, True)
    font_draw(10, -10, True)

    font_draw(15, -40, False)


def print_D():
    font_draw(0, 50, True)
    font_draw(15, 0, True)
    font_draw(15, -25, True)
    font_draw(-15, -25, True)
    font_draw(-15, 0, True)
    font_draw(40, 0, False)


def print_E():
    font_draw(0, 50, True)
    font_draw(25, 0, True)
    font_draw(-32, -30, False)
    font_draw(30, 0, True)
    font_draw(-30, -20, False)
    font_draw(30, 0, True)
    font_draw(15, 0, False)


def print_F():
    font_draw(0, 50, True)
    font_draw(30, 0, True)
    font_draw(-35, -23, False)
    font_draw(22, 0, True)
    font_draw(15, -27, False)

def print_G():
    font_draw(30, 55, False)
    font_draw(-30, 0, True)
    font_draw(0, -50, True)
    font_draw(30, 0, True)
    font_draw(0, -10, True)
    font_draw(0, 10, True)
    font_draw(0, 20, True)
    font_draw(-16, 0, True)
    font_draw(16+margin, -27, False)


def print_H():
    font_draw(0, 50, True)
    font_draw(0, -25, False)
    font_draw(30, 0, True)
    font_draw(0, 25, False)
    font_draw(0, -50, True)
    font_draw(15, 0, False)


def print_I():
    font_draw(0, 50, True)
    font_draw(17, -50, False)

def print_J():
    font_draw(0, 20, False)
    font_draw(0, -20, True)
    font_draw(25, 0, True)
    font_draw(0, 50, True)
    font_draw(-20, 0, True)
    font_draw(35, -50, False)


def print_K():
    font_draw(0, 50, True)
    font_draw(25, 0, False)
    font_draw(-30, -25, True)
    font_draw(30, -25, True)
    font_draw(15, 0, False)


def print_L():
    font_draw(0, 50, False)
    font_draw(0, -50, True)
    font_draw(25, 0, True)
    font_draw(15, 0, False)


def print_M():
    font_draw(0, 50, True)
    font_draw(15, -25, True)
    font_draw(15, 25, True)
    font_draw(0, -50, True)
    font_draw(15, 0, False)

def print_N():
    font_draw(0, 50, True)
    font_draw(30, -50, True)
    font_draw(0, 50, True)
    font_draw(15, -50, False)


def print_O():
    font_draw(0, 50, True)
    font_draw(30,0, True)
    font_draw(0, -50, True)
    font_draw(-32, 0, True)
    font_draw(32+margin, 0, False)


def print_P():
    font_draw(0, 50, True)
    font_draw(25, 0, True)
    font_draw(0, -20, True)
    font_draw(-25, 0, True)
    font_draw(25+margin, -30, False)


def print_Q():
    font_draw(0, 50, True)
    font_draw(30, 0, True)
    font_draw(0, -50, True)
    font_draw(-32, 0, True)
    font_draw(15, 15, False)
    font_draw(25, -25, True)
    font_draw(margin, 10, False)


def print_R():
    font_draw(0, 50, True)
    font_draw(25, 0, True)
    font_draw(0, -20, True)
    font_draw(-25, 0, True)
    font_draw(25, -30, True)
    font_draw(margin, 0, False)


def print_S():
    font_draw(30, 50, False)
    font_draw(-30, 0, True)
    font_draw(0, -25, True)
    font_draw(30, 0, True)
    font_draw(0, -25, True)
    font_draw(-30, 0, True)
    font_draw(30+margin, 0, False)


def print_T():
    font_draw(15, 0, False)
    font_draw(0, 50, True)
    font_draw(-20, 0, False)
    font_draw(30, 0, True)
    font_draw(margin, -50, False)


def print_U():
    font_draw(0, 50, False)
    font_draw(0, -50, True)
    font_draw(30, 0, True)
    font_draw(0, 50, True)
    font_draw(margin, -50, False)


def print_V():
    font_draw(0, 50, False)
    font_draw(15, -50, True)
    font_draw(15, 50, True)
    font_draw(margin, -50, False)


def print_W():
    font_draw(0, 50, False)
    font_draw(10, -50, True)
    font_draw(5, 20, True)
    font_draw(5, -20, True)
    font_draw(10, 50, True)
    font_draw(margin, -50, False)


def print_X():
    font_draw(30, 50, True)
    font_draw(-30, 0, False)
    font_draw(30, -50, True)
    font_draw(margin, 0, False)


def print_Y():
    font_draw(0, 50, False)
    font_draw(15, -25, True)
    font_draw(0, -25, True)
    font_draw(0, 26, False)
    font_draw(15, 25, True)
    font_draw(margin, -50, False)


def print_Z():
    font_draw(0, 50, False)
    font_draw(30, 0, True)
    font_draw(-30, -50, True)
    font_draw(30, 0, True)
    font_draw(margin, 0, False)


def print_dot():
    font_draw(0, 5, True)
    font_draw(5, 0, True)
    font_draw(0, -5, True)
    font_draw(-5, 0, True)
    font_draw(5, 0, False)


def print_space():
    draw.penMove(50, 0, False)


def print_linebreak():
    x = draw.penPositionX
    font_draw(-x, 0, False)
    font_draw(0, -60, False)

draw.feedIn()
getText()
draw.feedOut()
